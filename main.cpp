// $ g++ -std=c++11 -D_TARCER_ENABLED -I. main.cpp longfilename.cpp && ./a.out

#include "traceperiment/tracer.hpp"
#include "longfilename.hpp"
#include "verylongfilename.hpp"

int main()
{
    TRACER_INF("Hello traceperiment");
    verylongfilename();
    Longfilename a;
    a.call_method();
    a.call_method_inline();
}
