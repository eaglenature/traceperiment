#include "traceperiment/tracer.hpp"
#include "longfilename.hpp"

Longfilename::Longfilename()
{
    TRACER_DBG("Longfilename::Longfilename");
}

Longfilename::~Longfilename()
{
    TRACER_DBG("Longfilename::~Longfilename");
}

void Longfilename::call_method()
{
    TRACER_ERR("Longfilename::call_method null pointer");
}