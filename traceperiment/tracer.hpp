#pragma once

#ifdef _TARCER_ENABLED
#include <iostream>

#define TRACER_LOG(verbosity, expression, timeinfo, fileinfo) \
do {                                                          \
    std::cout << "[" << timeinfo << "]"                       \
              << "["<< #verbosity << "] "                     \
              << fileinfo                                     \
              << expression                                   \
              << std::endl;                                   \
} while (0)


#define TARCER_LOG_TIME(verbosity, expression) \
    TRACER_LOG(verbosity, expression, __DATE__ << " " << __TIME__, "")

#define TARCER_LOG_TIME_FILE(verbosity, expression) \
    TRACER_LOG(verbosity, expression, __DATE__ << " " << __TIME__, __FILE__ << ":" << __LINE__ << " ")

#define TRACER_DBG(expression) TARCER_LOG_TIME(DBG, expression)
#define TRACER_INF(expression) TARCER_LOG_TIME(INF, expression)
#define TRACER_ERR(expression) TARCER_LOG_TIME(ERR, expression)

#else

#define TRACER_LOG(verbosity, expression, timeinfo, fileinfo) 
#define TARCER_LOG_TIME(verbosity, expression)
#define TARCER_LOG_TIME_FILE(verbosity, expression)

#define TRACER_DBG(expression)
#define TRACER_INF(expression) 
#define TRACER_ERR(expression)

#endif
