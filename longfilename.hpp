#pragma once

#include "traceperiment/tracer.hpp"

class Longfilename
{
public:
    Longfilename();
    ~Longfilename();

    void call_method_inline()
    {
        TRACER_INF("Longfilename::call_method_inline processing " << 123);
    }

    void call_method();
};